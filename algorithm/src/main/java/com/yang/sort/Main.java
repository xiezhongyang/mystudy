package com.yang.sort;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int nums[] = new int[]{2,1,5,9,4,3};

        //冒泡排序
        System.out.println(String.format("冒泡排序结果:%s", Arrays.toString(ComparSort.BubbleSort(nums))));

        //选择排序
        System.out.println(String.format("选择排序结果:%s",Arrays.toString(ComparSort.SelectionSort(nums))));

        //插入排序
        System.out.println(String.format("插入排序结果:%s",Arrays.toString(ComparSort.InsertionSort(nums))));

        //希尔排序
        System.out.println(String.format("希尔排序结果:%s",Arrays.toString(ComparSort.ShellSort(nums))));
    }
}
