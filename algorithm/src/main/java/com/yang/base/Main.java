package com.yang.base;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        //两数之和
        int nums[] = new int[]{2, 7, 11, 15};
        int target = 9;
        int[] num = Solution.towSum(nums,target);
        int[] numnew = Solution.twoSumNew(nums,target);
        System.out.println(Arrays.toString(num));
        System.out.println(Arrays.toString(numnew));

    }

}
