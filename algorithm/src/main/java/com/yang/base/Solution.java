package com.yang.base;

import java.util.HashMap;
import java.util.Map;

public class Solution {

    /**
     * 给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。
     * @param nums 输入数组 [2,7,11,15]
     * @param target 目标值 9
     * @return 返回下标
     */
    public static int[] towSum(int[] nums,int target){
        int num[] = new int[2];
        for(int a=0;a<nums.length;a++){
            for(int b=a+1;b<nums.length;b++){
                if((nums[a]+nums[b])==target){
                    num[0] = a;
                    num[1] = b;
                }
            }
        }
        return num;
    }

    /**
     * 两数之和优化解：
     * 利用哈希表，且只遍历一次哈希表的方式，在执行时间及内存上都进行了极大的优化
     * @param nums 输入数组 [2,7,11,15]
     * @param target 目标值 9
     * @return 返回下标
     */
    public static int[] twoSumNew(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int com = target - nums[i];
            if(map.containsKey(com) && map.get(com)!=i){
                return new int[]{map.get(com),i};
            }
            map.put(nums[i], i);
        }
        throw new IllegalArgumentException("No two sum solution");
    }
}
